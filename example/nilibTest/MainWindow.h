﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE


class QTDMFile;
class QStandardItemModel;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionOpenTdms_triggered();

private:
    Ui::MainWindow *ui;
    QTDMFile *m_file;
    QStandardItemModel *m_model;
};
#endif // MAINWINDOW_H
