﻿#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "QTDMFile.h"
#include <QFileDialog>
#include <QStandardItemModel>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_file(nullptr)
{
    ui->setupUi(this);
    m_file = new QTDMFile();
    m_model = new QStandardItemModel(this);
    ui->treeView->setModel(m_model);
}


MainWindow::~MainWindow()
{
    delete ui;
    delete m_file;
}


void MainWindow::on_actionOpenTdms_triggered()
{
    m_model->clear();
    m_model->setHorizontalHeaderLabels({ QStringLiteral("分组-通道") });
    QString fp = QFileDialog::getOpenFileName(this, QStringLiteral("打开tdms文件"), QString(), tr("tdms文件 (*.tdms)"));

    if (fp.isEmpty()) {
        return;
    }
    if (!m_file->openFile(fp, true)) {
        return;
    }
    QList<QTDMGroup *> groups = m_file->getGroups();

    for (QTDMGroup *g : groups)
    {
        QStandardItem *gi = new QStandardItem(g->getName());
        QList<QTDMChannel *> channels = g->getChannels();
        for (QTDMChannel *c : channels)
        {
            QStandardItem *ci = new QStandardItem(c->getName());
            gi->appendRow(ci);
        }
        m_model->appendRow(gi);
    }
    ui->treeView->expandAll();
}
