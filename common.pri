CONFIG(debug, debug|release){
    contains(QT_ARCH, i386) {
        QTDM_BIN_DIR = $$PWD/bin_qt$$[QT_VERSION]_debug
    }else {
        QTDM_BIN_DIR = $$PWD/bin_qt$$[QT_VERSION]_debug_64
    }
}else{
    contains(QT_ARCH, i386) {
        QTDM_BIN_DIR = $$PWD/bin_qt$$[QT_VERSION]_release
    }else {
        QTDM_BIN_DIR = $$PWD/bin_qt$$[QT_VERSION]_release_64
    }
}
QTDM_SRC_DIR = $$PWD/src # 源代码路径
QTDM_DLL_DIR = $$PWD/dll # 第三方库路径
# 把当前目录作为include路径
INCLUDEPATH += $$PWD
