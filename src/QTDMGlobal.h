﻿#ifndef QTDMGLOBAL_H
#define QTDMGLOBAL_H

#if defined(QTDM_USE_SHARED_LIB)
#if defined(QTDM_MAKE_LIB)      // create a DLL library
#define QTDM_EXPORT	Q_DECL_EXPORT
#else                           // use a DLL library
#define QTDM_EXPORT	Q_DECL_IMPORT
#endif
#else
#define QTDM_EXPORT
#endif

#define DDC_MY_TYPE_NO		-1
#define DDC_MY_TYPE_FILE	0
#define DDC_MY_TYPE_GROUP	1
#define DDC_MY_TYPE_CHANNEL	2
#endif // QTDMGLOBAL_H
