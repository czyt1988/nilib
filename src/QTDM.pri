include($$PWD/../common.pri)
INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += $$PWD/nilibddc.h \
        $$PWD/QTDMChannelTableModel.h \
        $$PWD/QTDMGlobal.h \
        $$PWD/QTDMFile.h \
        $$PWD/QTDMGroup.h \
        $$PWD/QTDMChannel.h

SOURCES += \
        $$PWD/QTDMChannelTableModel.cpp \
        $$PWD/QTDMFile.cpp \
        $$PWD/QTDMGroup.cpp \
        $$PWD/QTDMChannel.cpp


LIBS += -L$${QTDM_DLL_DIR} -lnilibddc
LIBS += -luser32
